-- Set globals
Object = require "src.lib.classic"
Loader = require "src.lib.love-loader"
LoveKeys = require "src.lib.LoveKeys"

-- love libs 
SSM = require "src.lib.StackingSceneMgr"
CM = require "src.lib.CameraMgr"

bf = require "src.lib.breezefield"

-- utils
require "src.utils"

function love.load()
    Loader.start(function()
        finishedLoading = true
    end)
    SSM.setPath("src/scenes")
    SSM.add("scene")
end

function love.update(dt)
    if not finishedLoading then
        Loader.update()
    end
    CM.update(dt)
    SSM.update(dt)
    -- Do not remove this! Input updates
    LoveKeys.update(dt)
end

function love.draw()
    CM.attach()
    SSM.draw()
    CM.detach()
end

function love.keypressed(key)
    SSM.keypressed(key)
end

function love.keyreleased(key)
    SSM.keyreleased(key)
end