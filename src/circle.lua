require("src.lib.classic")

circle = {
    _s = 0,
    _rgba = {
        r = 0,
        g = 0,
        b = 0,
        a = 0
    },
    _maxR = 0,

    -- Box2D Time
    _shape = nil,

    new = function(x, y, r, s)
        local self = setmetatable({}, Circle)
        self._maxR = r
        self._shape = love.physics.newCircleShape(x, y, 0)
        self._s = s
        return self
    end
}

circle.__index = circle
