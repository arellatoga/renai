assertCallback = function(cb)
    cb = cb or function() end
    assert(type(cb) == 'function', string.format("Callback error - wanted function is of type %s", type(cb)))
end