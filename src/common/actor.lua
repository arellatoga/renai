--[[
Any in game object that may be interacted with through
player input, collisions, or whatever,
is an Actor.
--]]

require "src.utils"

Actor = Object:extend()

Actor.collider = nil
Actor.velocity = 0

function Actor:variableperform(element, success, fail)
    assert(success or fail, "Callbacks cannot be both nil")
    assert(type(element) == 'string', string.format("Require error - wanted element type is %s", type(element)))
    assertCallback(success)
    assertCallback(fail)
    if self[element] ~= nil then
        return success()
    elseif fail ~= nil then
        return fail()
    end
end

function Actor:new()
    self.velocity = 500
    self.prepareInput()
end

function Actor:newcollider(world)
    self.collider = bf.Collider.new(world, "Rectangle", 0, 0, 10, 10)
    self.collider:setType("dynamic")
end

function Actor:setcollider(collider)
    self.collider = collider
end

function Actor:update(dt)
    local x = 0
    local y = 0
    if LoveKeys.d.Held and LoveKeys.a.Held then
    elseif LoveKeys.d.Held then
        x = self.velocity
    elseif LoveKeys.a.Held then
        x = -self.velocity
    end
    if LoveKeys.w.Held and LoveKeys.s.Held then
    elseif LoveKeys.w.Held then
        y = -self.velocity
    elseif LoveKeys.s.Held then
        y = self.velocity
    end

    self.collider:setLinearVelocity(x, y)
end

function Actor:prepareInput()
    LoveKeys.RegisterKey("w")
    LoveKeys.RegisterKey("a")
    LoveKeys.RegisterKey("s")
    LoveKeys.RegisterKey("d")
end

function Actor:keypressed(key)
    LoveKeys.keypressed(key)
end

function Actor:keyreleased(key)
    LoveKeys.keyreleased(key)
end

function Actor:draw()
end

Actor.__index = Actor