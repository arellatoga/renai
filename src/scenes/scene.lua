require("src.common.actor")

local scene = {}
local actors = {}
local world = {}

local insertActor = function(actor) 
    actors[#actors+1] = actor
end

local resolveActors = function(method, ...)
    for index, actor in ipairs(actors) do
        if type(actor[method]) == 'function' then
            actor[method](actor, unpack(arg))
        end
    end
end

function scene.load()
    world = bf.World:new(0, 0, true)
    actor = Actor()
    actor:newcollider(world)
    collider = bf.Collider.new(world, "Rectangle", -100, 100, 10, 200)
    collider:setType("static")
    insertActor(actor)
end

function scene.update(dt)
    world:update(dt)
    resolveActors('update', dt)
end

function scene.draw()
    resolveActors('draw')
    world:draw()
end

function scene.keypressed(key)
    actor:keypressed(key)
end

function scene.keyreleased(key)
    actor:keyreleased(key)
end

return scene